<?php

namespace App\Http\Controllers;
use App\Channel;
use Illuminate\Http\Request;
use Session;
class ChannelsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('channels.index')->with('channels',Channel::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('channels.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request,[
            'channel'=>'required'
        ]);

        Channel::create([
            'title'=> $request->channel,
            'slug'=>str_slug($request->channel)
        ]);

        Session::flash('success',"Channel Created");

        return redirect()->route('channels.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        Channel::find($id);
        return view('channels.edit')->with('channel',Channel::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $chnnel=Channel::find($id);
        $chnnel->title=$request->channel;
        $chnnel->slug=str_slug($request->channel);
        $chnnel->save();

        Session::flash('success',"Channel edited successfully");
        return redirect()->route('channels.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Channel::destroy($id);
        Session::flash('success','Channel Deleted');
        return redirect()->route('channels.index');
    }
}
