<?php

namespace App\Http\Controllers;
use App\Reply;
use App\User;
use Auth;
use Notification;
use App\Discussion;
use Illuminate\Http\Request;
use Session;

class DiscussionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('discussion/discuss');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate(request(),[
            'channel_id'=>'required',
            'content'=>'required',
            'title'=>'required'
        ]);

        $discussion= Discussion::create([
            'title'=>$request->title,
            'channel_id'=>$request->channel_id,
            'content'=>$request->content,
            'user_id'=>Auth::id(),
            'slug'=>str_slug($request->title)
        ]);

        Session::flash('success','Discussion created successfully');

        return redirect()->route('discussion',['slug'=>$discussion->slug]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        //
        $discussion =Discussion::where('slug',$slug)->first();
        $best_answer=$discussion->replies()->where('best_answer',1)->first();
        return view('discussion.show')->with('d',$discussion)->with('best_answer',$best_answer);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($slug)
    {
        //
        return view('discussion.edit',['discussion'=>Discussion::where('slug',$slug)->first()]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
           'content'=>'required'
        ]);

        $d=Discussion::find($id);
        $d->content=$request->content;
        $d->save();
        Session::flash('success','Post successfully updated');
        return redirect()->route('discussion',['slug'=>$d->slug]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function reply($id)
    {
        $d=Discussion::find($id);
        $reply=Reply::create([
            'user_id'=> Auth::id(),
            'discussion_id'=>$id,
            'content'=> request('reply'),
        ]);

        $watchers=array();
        foreach ($d->watchers as $watcher):
            array_push($watchers,User::find($watcher->user_id));
            endforeach;

            Notification::send($watchers,new \App\Notifications\NewReplyAdded($d));



        Session::flash('success','Comment posted successfully');
        return redirect()->back();
    }
}
