<?php

namespace App\Http\Controllers;
use Auth;
use App\Like;
use App\Reply;
use Illuminate\Http\Request;
use Session;

class LikesController extends Controller
{
    public function like($id)
    {
        Like::create([
           'reply_id'=>$id,
           'user_id'=>Auth::id()
        ]);

        Session::flash('success','You have liked a comment');
        return redirect()->back();
    }
    public function unlike($id)
    {
        $like=Like::where('reply_id',$id)->where('user_id',Auth::id())->first();
        $like->delete();

        Session::flash('success','You have unliked a comment');
        return redirect()->back();
    }
    public function best_answer($id)
    {
        $reply= Reply::find($id);
        $reply->best_answer=1;
        $reply->save();

        Session::flash('success','Reply has been set as best answer');
        return redirect()->back();
    }

    public function edit($id)
    {
        return view('replies.edit',['reply'=> Reply::find($id) ]);
    }

    public function update( Request $request, $id)
    {
        $this->validate($request,[
           'content'=>'required'
        ]);
        $reply=Reply::find($id);
        $reply->content=$request->content;
        $reply->save();

        Session::flash('success','Reply has been updated');
        return redirect()->route('discussion',['slug'=>$reply->discussion->slug]);
    }
}
