<?php

use Illuminate\Database\Seeder;
use App\Channel;
class ChannelsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $channel1=['title'=>'Laravel 1','slug'=>str_slug('Laravel')];
        $channel2=['title'=>'Laravel 2','slug'=>str_slug('Larave2')];
        $channel3=['title'=>'Laravel 3','slug'=>str_slug('Larave3')];
        $channel4=['title'=>'Laravel 4','slug'=>str_slug('Larave4')];
        $channel5=['title'=>'Laravel 5','slug'=>str_slug('Larave5')];
        $channel6=['title'=>'Laravel 6','slug'=>str_slug('Larave6')];
        $channel7=['title'=>'Laravel 7','slug'=>str_slug('Larave7')];
        $channel8=['title'=>'Laravel 8','slug'=>str_slug('Larave8')];
        $channel9=['title'=>'Laravel 9','slug'=>str_slug('Larave9')];

        Channel::create($channel1);
        Channel::create($channel2);
        Channel::create($channel3);
        Channel::create($channel4);
        Channel::create($channel5);
        Channel::create($channel6);
        Channel::create($channel7);
        Channel::create($channel8);
        Channel::create($channel9);
    }
}
