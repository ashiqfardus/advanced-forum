<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(Userstableseeder::class);
        $this->call(ChannelsTableSeeder::class);
        $this->call(DiscussiontableSeeder::class);
        $this->call(RepliesTableSeeder::class);
        $this->call(LikeTableSeeder::class);

    }
}
