<?php

use Illuminate\Database\Seeder;
use App\Discussion;
class DiscussiontableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $title1='Discussion title 1';
        $title2='Discussion title 2';
        $title3='Discussion title 3';
        $title4='Discussion title 4';

        $discussion1=[
          'title'=>$title1,
          'content'=>'Lorem Ipsum text',
          'channel_id'=>1,
          'user_id'=>1,
          'slug'=>str_slug($title1)
        ];
        $discussion2=[
            'title'=>$title2,
            'content'=>'Lorem Ipsum text',
            'channel_id'=>1,
            'user_id'=>1,
            'slug'=>str_slug($title2)
        ];
        $discussion3=[
            'title'=>$title3,
            'content'=>'Lorem Ipsum text',
            'channel_id'=>1,
            'user_id'=>2,
            'slug'=>str_slug($title3)
        ];
        $discussion4=[
            'title'=>$title4,
            'content'=>'Lorem Ipsum text',
            'channel_id'=>1,
            'user_id'=>2,
            'slug'=>str_slug($title4)
        ];
        Discussion::create($discussion1);
        Discussion::create($discussion2);
        Discussion::create($discussion3);
        Discussion::create($discussion4);
    }
}
