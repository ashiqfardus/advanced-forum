<?php

use Illuminate\Database\Seeder;

class LikeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $like1=[
            'user_id'=>1,
            'reply_id'=>1
        ];
        $like2=[
            'user_id'=>2,
            'reply_id'=>2
        ];

        \App\Like::create($like1);
        \App\Like::create($like2);
    }
}
