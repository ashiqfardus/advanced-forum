<?php

use Illuminate\Database\Seeder;
use App\Reply;
class RepliesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $reply1=[
        'user_id'=>1,
        'discussion_id'=>1,
        'content'=>'Reply test'
        ];
        $reply2=[
            'user_id'=>1,
            'discussion_id'=>2,
            'content'=>'Reply test'
        ];
        $reply3=[
            'user_id'=>2,
            'discussion_id'=>3,
            'content'=>'Reply test'
        ];
        $reply4=[
            'user_id'=>2,
            'discussion_id'=>4,
            'content'=>'Reply test'
        ];
        Reply::create($reply1);
        Reply::create($reply2);
        Reply::create($reply3);
        Reply::create($reply4);
    }
}
