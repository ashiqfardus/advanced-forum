@extends('layouts.app')

@section('content')

    @foreach($discussions as $d)
        <div class="card" style="margin-top: 10px;">
            <div class="card-header">
                <img src="{{$d->user->avatar}}" alt="{{$d->user->name}}" width="40px" height="40px">&nbsp;&nbsp;&nbsp;
                <span class="text-center">
                            {{$d->user->name}},  <b>{{$d->created_at->diffForHumans()}}</b>
                        </span>
                @if($d->hasBestAnswer())
                    <span class="btn btn float-right btn-success btn-sm" style="margin-left: 5px;">Closed</span>
                @else
                    <span class="btn btn float-right btn-danger btn-sm" style="margin-left: 5px;">Open</span>
                @endif
                <a href="{{route('discussion',['slug'=>$d->slug])}}" class="btn btn-primary btn-sm float-right">View</a>
            </div>
            <div class="card-body">
                <h4 class="text-center">
                    {{$d->title}}
                </h4>
                <p class="text-center">
                    {{str_limit($d->content, 100)}}
                </p>
            </div>
            <div class="card-footer">
                <span>
                    {{$d->replies->count()}} Replies
                </span>
                <a href="{{route('channel',['slug'=>$d->channel->slug ])}}" class="btn btn-primary btn-sm float-right">{{$d->channel->title}}</a>
            </div>
        </div>
    @endforeach


@endsection
