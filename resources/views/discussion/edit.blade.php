@extends('layouts.app')

@section('content')


    <div class="card">
        <div class="card-header text-center">Update a discussion</div>

        <div class="card-body">
            <form action="{{route('discussion.update',['id'=>$discussion->id])}}" method="post">
                {{csrf_field()}}

                <div class="form-group">
                    <label for="content">Ask a question</label>
                    <textarea name="content" id="content" cols="30" rows="5" class="form-control">{{$discussion->content}}</textarea>
                </div>
                <div class="form-group">
                    <button class="btn btn-success" type="submit"> Submit</button>
                </div>

            </form>
        </div>
    </div>

@endsection
