@extends('layouts.app')

@section('content')
        <div class="card" style="margin-top: 10px;">
            <div class="card-header">
                <img src="{{$d->user->avatar}}" alt="{{$d->user->name}}" width="40px" height="40px">&nbsp;&nbsp;&nbsp;
                <span class="text-center">
                   {{$d->user->name}},  <b>{{$d->created_at->diffForHumans()}}</b>
                </span>
                @if($d->hasBestAnswer())
                    <span class="btn btn float-right btn-success btn-sm" style="margin-left: 5px;">Closed</span>
                @else
                    <span class="btn btn float-right btn-danger btn-sm" style="margin-left: 5px;">Open</span>
                @endif

                @if(Auth::id()==$d->user->id)
                    <a href="{{route('discussion.edit',['slug'=>$d->slug])}}" class="btn btn-info btn-sm float-right" style="margin-left: 5px;">Edit</a>
                    @endif

                @if($d->is_being_watched_by_auth_user())
                    <a href="{{route('discussion.unwatch',['id'=>$d->id])}}" class="btn btn-primary btn-sm float-right">Unwatch</a>
                    @else
                    <a href="{{route('discussion.watch',['id'=>$d->id])}}" class="btn btn-primary btn-sm float-right">Watch</a>
                @endif

            </div>
            <div class="card-body">
                <h4 class="text-center">
                    {{$d->title}}
                </h4>
                <p class="text-center">
                    {{$d->content}}
                </p>

                @if($best_answer)
                <hr>
                <div class="text-center">
                    <div class="card-body text-center">
                        <h4>Best Answer</h4>
                    </div>
                    <div class="card-header">
                        <img src="{{$best_answer->user->avatar}}" alt="{{$best_answer->user->name}}" width="40px" height="40px">&nbsp;&nbsp;&nbsp;
                        <span class="text-center">
                            <b>{{$best_answer->user->name}}</b>
                        </span>
                    </div>
                    <div class="card-body">
                        <p>
                            {{$best_answer->content}}
                        </p>
                    </div>
                </div>
                @endif

            </div>
            <div class="card-footer">
                <span>
                    {{$d->replies->count()}} Replies
                </span>
                <a href="{{route('channel',['slug'=>$d->channel->slug ])}}" class="btn btn-primary btn-sm float-right">{{$d->channel->title}}</a>
            </div>
        </div>

        <div class="card"  style="margin-top: 10px">
            <div class="card-header">
                Comments
            </div>
        </div>
    @foreach($d->replies as $r)
        <div class="card">
            <div class="card-header">
                <img src="{{$r->user->avatar}}" alt="{{$r->user->name}}" width="40px" height="40px">&nbsp;&nbsp;&nbsp;
                <span class="text-center">
                    {{$r->user->name}},  <b>{{$r->created_at->diffForHumans()}}</b>
                </span>
                @if(!$best_answer)
                    @if(Auth::id()==$d->user->id)
                        <a href="{{route('discussion.best.answer',['id'=>$r->id])}}" class="btn btn-sm btn-primary float-right">
                            Mark as best answer
                        </a>
                        @endif

                        @if(Auth::id()==$r->user->id)
                           @if(!$r->best_answer)
                                <a href="{{route('replies.edit',['id'=>$r->id])}}" class="btn btn-sm btn-info float-right" style="margin-right: 8px">
                                    Edit
                                </a>
                           @endif
                        @endif
                @endif
            </div>
            <div class="card-body">
                <p>
                    {{$r->content}}
                </p>
            </div>
            <div class="card-footer">
                @if($r->is_liked_by_auth_user())
                    <a href="{{route('reply.unlike',['id'=>$r->id])}}" class="btn btn-danger btn-sm">Unlike <span class="badge badge-light">{{$r->likes->count()}}</span></a>
                    @else
                    <a href="{{route('reply.like',['id'=>$r->id])}}" class="btn btn-success btn-sm">Like <span class="badge badge-light">{{$r->likes->count()}}</span></a>
                @endif
            </div>
        </div>
    @endforeach

    @if(Auth::check())
        <div class="card">
            <div class="card-header">
                Post e Comment
            </div>
            <div class="card-body">
                <form class="form-group" action="{{route('discussion.reply',['id'=>$d->id])}}" method="post">
                    {{csrf_field()}}
                    <div class="form-group">
                        <textarea name="reply" id="reply" cols="30" rows="5" class="form-control"></textarea>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-success float-right">Leave a comment</button>
                    </div>
                </form>
            </div>
        </div>
    @else
        <div class="text-center">
            Sign in to Reply
        </div>

    @endif

@endsection
